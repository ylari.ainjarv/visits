package ee.bcs.valiit.visits;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

//@Data
public class Visit implements Comparable<Visit> {

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private Date date;
    private int count;

    public Visit() {}

    public Visit(Date date, int count) {
        this.date = date;
        this.count = count;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date) + ", " + count;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Visit otherVisit = (Visit) object;
        boolean dateIsSame = this.date.toString().equals(otherVisit.getDate().toString());
        boolean countIsSame = this.count == otherVisit.getCount();
        return dateIsSame && countIsSame;
    }

    @Override
    public int compareTo(Visit visit) {
        if (count < visit.getCount()) {
            return -1;
        } else if (count > visit.getCount()) {
            return 1;
        }
        return 0;
    }

}
