package ee.bcs.valiit.visits;

import javax.print.DocFlavor;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class VisitsApp {

    public static void main(String[] args) {
        if (args.length > 0) {
            try {
                String filePath = args[0];
                Path path = Paths.get(filePath);
                List<String> fileLines = Files.readAllLines(path);
                Visit[] visits = new Visit[fileLines.size()];
                for (int i = 0; i < fileLines.size(); i++) {
                    String[] lineParts = fileLines.get(i).split(", ");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = format.parse(lineParts[0]);
                    int count = Integer.parseInt(lineParts[1]);
                    Visit visit = new Visit(date, count);
                    visits[i] = visit;
                }
                Arrays.sort(visits);
                for (Visit visit : visits) {
                    System.out.println(visit);
                }
                System.out.println("Kõige vähem külastajaid oli " + visits[0]);
                System.out.println("Kõige rohkem oli külastajaid " + visits[visits.length - 1]);

                Date date = new Date();
                Visit a = new Visit(date, 100);
                Visit b = new Visit(date, 100);

                System.out.println("Kas on võrdsed? " + a.equals(b));



            } catch (IOException e) {
                System.err.println("Faili lugemine ebaõnnestus!");
            } catch (ParseException e) {
                System.err.println("Kuupäeva formaat failis on vale!");
                e.printStackTrace();
            }
        } else {
            System.out.println("Faili asukoht on kohustuslik!");
        }
    }

}
